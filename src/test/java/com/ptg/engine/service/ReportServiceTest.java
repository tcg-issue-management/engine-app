package com.ptg.engine.service;

import net.sf.jasperreports.engine.JasperPrint;
import org.apache.poi.util.IOUtils;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.*;

import com.protoss.ims.engine.service.ReportService;
import com.protoss.ims.engine.service.Impl.ReportServiceImpl;
import com.ptg.engine.AbstractApplicationTests;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
@Transactional
public class ReportServiceTest extends AbstractApplicationTests {
    private static final Logger LOGGER = LoggerFactory.getLogger(ReportServiceTest.class);

    @Autowired
    ReportServiceImpl reportService;

  

  @Test
  public void testReport(){
    try{
    
      XSSFWorkbook wb = reportService.generateReport(Long.valueOf(1));
            try {

                if(wb != null){
                    FileOutputStream fos = new FileOutputStream(new File("F:\\ims\\reportTest\\test.xlsx"));
                    wb.write(fos);
                    fos.close();
                }

            } catch (Exception e) {
                e.getMessage();
            }

    }catch(Exception e){
      LOGGER.error("error msg : {}",e.getMessage());
      throw new RuntimeException(e);
    }
  }



}
