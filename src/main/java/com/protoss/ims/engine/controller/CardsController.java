package com.protoss.ims.engine.controller;

import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.protoss.ims.engine.model.Cards;
import com.protoss.ims.engine.model.Project;
import com.protoss.ims.engine.payload.ProjectResponse;
import com.protoss.ims.engine.repository.CardsRepository;
import com.protoss.ims.engine.repository.ProjectRepository;
import com.protoss.ims.engine.service.CardsService;

import org.springframework.http.HttpHeaders;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cards")
public class CardsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    CardsService cardsService;

    @GetMapping("/findCardsByProjectId")
    public ResponseEntity<String> getAllProject(@RequestParam("id")Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        try{
            List<Map<String,Object>> result = cardsService.findCardList(id);


            return new ResponseEntity<String>(new JSONSerializer()
                    .exclude("*.class")
                    .exclude("*.projectGroup")
                    .deepSerialize(result), headers, HttpStatus.OK);

        }catch (Exception e){
            LOGGER.error("Exception[CardsController - findCardsByProjectId] Msg : {}",e.getMessage());
            e.printStackTrace();
            throw new RuntimeException();
        }

    }

    @GetMapping("/findCardsByProjectIdAndStatus")
    public ResponseEntity<String> findCardsByProjectIdAndStatus(@RequestParam("id")Long id,@RequestParam("status")String status) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        try{
            List<Map<String,Object>> result = cardsService.findCardsByProjectIdAndStatus(id,status);
            return new ResponseEntity<String>(new JSONSerializer()
                    .exclude("*.class")
                    .exclude("*.projectGroup")
                    .deepSerialize(result), headers, HttpStatus.OK);
        }catch (Exception e){
            LOGGER.error("Exception[CardsController - findCardsByProjectId] Msg : {}",e.getMessage());
            e.printStackTrace();
            throw new RuntimeException();
        }

    }



    @RequestMapping(value = "/saveCard",headers = "Accept=application/json")
    ResponseEntity<String> saveProject(@RequestBody String data){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
    
        try {
            System.out.println("Json Data = ["+data+"] ");
            LOGGER.info("Json Data [{}] ",data);
    
            cardsService.saveCard(data);
            headers.add("statusValidate","0");
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").serialize("")), headers, HttpStatus.OK);
        }catch(IllegalArgumentException e) {
            LOGGER.error("IllegalArgumentException Msg : "+e.getMessage());
            e.printStackTrace();
            headers.add("statusValidate","-1");
            return new ResponseEntity<String>((new JSONSerializer().serialize("-1")), headers, HttpStatus.OK);
        }
        catch (Exception e) {
            LOGGER.error("Exception Msg : "+e.getMessage());
            e.printStackTrace();
            headers.add("statusValidate","-1");
            return new ResponseEntity<String>((new JSONSerializer().serialize("-1")), headers, HttpStatus.OK);
        }
    }






}
