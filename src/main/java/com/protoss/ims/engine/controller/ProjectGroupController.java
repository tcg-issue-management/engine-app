package com.protoss.ims.engine.controller;

import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.protoss.ims.engine.model.Project;
import com.protoss.ims.engine.model.ProjectGroup;
import com.protoss.ims.engine.service.ProjectGroupService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/groups")
public class ProjectGroupController {

    static final Logger LOGGER = LoggerFactory.getLogger(ProjectGroupController.class);

    @Autowired
    private ProjectGroupService projectGroupService;



    @GetMapping(value="/{id}",produces = "application/json; charset=utf-8")
    public String findProjectGroupAndCards(@PathVariable Long id) {

        LOGGER.info("===== findProjectGroupAndCards =====");
        List<ProjectGroup> projectGroupsList = projectGroupService.findProjectGroupAndCards(id);

        LOGGER.info("projectGroupsList = {} ",projectGroupsList.size());

        return (new JSONSerializer()
                .exclude("*.class")
                .include("id")
                .include("createdBy")
                .include("createdDate")
                .include("updateBy")
                .include("updateDate")
                .include("realId")
                .include("displayName")
                .include("project.projectName")
                .include("cards.id")
                .include("cards.msg")
                .include("cards.sender")
                .include("cards.createdDate")
                .exclude("*")
                .deepSerialize(projectGroupsList));
    }



    @RequestMapping(value = "/saveProjectGroup",headers = "Accept=application/json")
    ResponseEntity<String> saveProject(@RequestBody String data){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        List listIdChangeProduct = new ArrayList();
        try {
            System.out.println("Json Data = "+data);
            String[] datSplit = data.split("&");

            String code = datSplit[0].split("=")[1];
            Long projectId = Long.valueOf(datSplit[1].split("=")[1]);
            projectGroupService.saveProjectGroup(code,projectId);
            headers.add("statusValidate","0");
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").serialize(listIdChangeProduct)), headers, HttpStatus.OK);
        }catch(IllegalArgumentException e) {
            LOGGER.error("IllegalArgumentException Msg : "+e.getMessage());
            e.printStackTrace();
            headers.add("statusValidate","-1");
            return new ResponseEntity<String>((new JSONSerializer().serialize("-1")), headers, HttpStatus.OK);
        }
        catch (Exception e) {
            LOGGER.error("Exception Msg : "+e.getMessage());
            e.printStackTrace();
            headers.add("statusValidate","-1");
            return new ResponseEntity<String>((new JSONSerializer().serialize("-1")), headers, HttpStatus.OK);
        }
    }

}
