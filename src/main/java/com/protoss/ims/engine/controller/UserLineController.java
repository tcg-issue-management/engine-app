package com.protoss.ims.engine.controller;

import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.protoss.ims.engine.service.CardsService;
import com.protoss.ims.engine.service.UserLineService;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/userLine")
public class UserLineController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserLineService userLineService;

    @GetMapping("/findUserLineByProjectId")
    public ResponseEntity<String> getAllProject(@RequestParam("id")Long id) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        try{
            List<Map<String,Object>> result = userLineService.findUserLing(id);


            return new ResponseEntity<String>(new JSONSerializer()
                    .exclude("*.class")
                    .exclude("*.projectGroup")
                    .deepSerialize(result), headers, HttpStatus.OK);

        }catch (Exception e){
            LOGGER.error("Exception[CardsController - findCardsByProjectId] Msg : {}",e.getMessage());
            e.printStackTrace();
            throw new RuntimeException();
        }

    }
}
