package com.protoss.ims.engine.controller;

import flexjson.JSONSerializer;
import jdk.nashorn.internal.parser.JSONParser;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.protoss.ims.engine.exception.ResourceNotFoundException;
import com.protoss.ims.engine.model.Project;
import com.protoss.ims.engine.payload.ProjectResponse;
import com.protoss.ims.engine.repository.ProjectRepository;
import com.protoss.ims.engine.service.ProjectService;

import javax.transaction.Transactional;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("/api")
public class ProjectController {


    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);

    @Autowired
    ProjectRepository projectRepository;

    @GetMapping("/project")
    public List<ProjectResponse> getAllProject() {
        List<Project> projectList = projectRepository.findAll();

        List<ProjectResponse> projectResponseList = new ArrayList<>();
        if(projectList.size()>0){
            for(Project p :projectList) {
                ProjectResponse projectResponse = new ProjectResponse(p.getId(),p.getCreatedBy(),p.getProjectName());
                projectResponseList.add(projectResponse);
            }
        }

        return projectResponseList;
    }



    @RequestMapping(value = "/saveProject",headers = "Accept=application/json")
    ResponseEntity<String> saveProject(@RequestBody String data){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        List listIdChangeProduct = new ArrayList();
        try {
            System.out.println("Json Data = "+data);
            String[] datSplit = data.split("&");
            String projectName = datSplit[0].split("=")[1];
                Project project = new Project();
            project.setProjectName(projectName);
            projectRepository.saveAndFlush(project);
            headers.add("statusValidate","0");
            return new ResponseEntity<String>((new JSONSerializer().exclude("*.class").serialize(listIdChangeProduct)), headers, HttpStatus.OK);
        }catch(IllegalArgumentException e) {
            LOGGER.error("IllegalArgumentException Msg : "+e.getMessage());
            e.printStackTrace();
            headers.add("statusValidate","-1");
            return new ResponseEntity<String>((new JSONSerializer().serialize("-1")), headers, HttpStatus.OK);
        }
        catch (Exception e) {
            LOGGER.error("Exception Msg : "+e.getMessage());
            e.printStackTrace();
            headers.add("statusValidate","-1");
            return new ResponseEntity<String>((new JSONSerializer().serialize("-1")), headers, HttpStatus.OK);
        }
    }


}
