package com.protoss.ims.engine.controller;

import flexjson.JSONSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.protoss.ims.engine.model.Priority;
import com.protoss.ims.engine.model.Project;
import com.protoss.ims.engine.payload.ProjectResponse;
import com.protoss.ims.engine.repository.PriorityRepository;
import com.protoss.ims.engine.repository.PriorityRepositoryImpl;
import com.protoss.ims.engine.repository.ProjectRepository;
import com.protoss.ims.engine.service.PriorityService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/priority")
public class PriorityController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired

    PriorityRepository priorityRepository;

//    @GetMapping("/findPriorityLike")
//    public ResponseEntity<String> findPriorityLike(@RequestParam("hasText")String hasText,
//                                                   @RequestParam("flagActive")String flagActive,
//                                                   @RequestParam("responseValue")String responseValue,
//                                                   @RequestParam("firstResult")int firstResult,
//                                                   @RequestParam("maxResult")int maxResult) {
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("Content-Type", "application/json; charset=utf-8");
//
//        try{
//            List<Priority> priorityList = priorityRepository.findAllPriorityLike(hasText,flagActive,responseValue,firstResult,maxResult);
//
//            return new ResponseEntity<String>(new JSONSerializer().exclude("*.class").deepSerialize(priorityList), headers, HttpStatus.OK);
//        }catch (Exception e) {
//            LOGGER.error("error : [{}]", e.getMessage());
//            return new ResponseEntity<String>(new JSONSerializer().exclude("*.class").deepSerialize(null), headers, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }
//
//    @GetMapping("/findPriorityLikeSize")
//    public ResponseEntity<String> findPriorityLikeSize(@RequestParam("hasText")String hasText,
//                                                       @RequestParam("flagActive")String flagActive,
//                                                       @RequestParam("responseValue")String responseValue
//                                                       ) {
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("Content-Type", "application/json; charset=utf-8");
//
//        try{
//            Long priorityList = priorityRepository.findAllPriorityLikeSize(hasText,flagActive,responseValue);
//
//            return new ResponseEntity<String>(new JSONSerializer().exclude("*.class").deepSerialize(priorityList), headers, HttpStatus.OK);
//        }catch (Exception e) {
//            LOGGER.error("error : [{}]", e.getMessage());
//            return new ResponseEntity<String>(new JSONSerializer().exclude("*.class").deepSerialize(null), headers, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }


    @GetMapping("/findAll")
    public ResponseEntity<String> findAll() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");

        try{
            List<Priority> list = priorityRepository.findAll();
            return new ResponseEntity<String>(new JSONSerializer().exclude("*.class").deepSerialize(list), headers, HttpStatus.OK);
        }catch (Exception e) {
            LOGGER.error("error : [{}]", e.getMessage());
            return new ResponseEntity<String>(new JSONSerializer().exclude("*.class").deepSerialize(null), headers, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
