package com.protoss.ims.engine.controller;

import flexjson.JSONSerializer;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.protoss.ims.engine.service.ReportService;
import com.protoss.ims.engine.service.UserLineService;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/report")
public class ReportController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReportController.class);

    @Autowired
    ReportService reportService;


    @PostMapping("/generateReport")
    public ResponseEntity<String> generateReport(@RequestParam("id") Long id,
                                                 @RequestParam("startDate") String startDate,
                                                 @RequestParam("endDate") String endDate,
                                                 @RequestParam("status") String status,
                                                 HttpServletRequest request,
                                                 HttpServletResponse response
                                                 )throws ServletException, IOException {

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json; charset=utf-8");
        LOGGER.info("generateReport[Controller]");
        try {

            XSSFWorkbook workbook = reportService.generateReport(id,startDate,endDate,status);
            // XSSFWorkbook workbook = reportService.generateReportWithTemplate();
            if(workbook != null){
                workbook.write(response.getOutputStream());
                response.getOutputStream().flush();
            }
            headers.add("statusValidate","0");
            return new ResponseEntity<String>(headers, HttpStatus.OK);
        } catch (IllegalStateException e) {
            e.printStackTrace();
            LOGGER.error("generateReport[Controller] error msg : {}",e.getMessage());
            headers.add("statusValidate","-1");
            headers.add("errorMsg",e.getMessage());
            return new ResponseEntity<String>(e.getMessage(),headers, HttpStatus.OK);
        }
    }
   

   
}
