package com.protoss.ims.engine.util;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by nick on 12/10/2559.
 */
public class CommonUtil {

    private static SimpleDateFormat df = new SimpleDateFormat("dd-mm-yyyy");
    private static SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyy-mm--dd");
    private static final Logger LOGGER = LoggerFactory.getLogger(CommonUtil.class);

    static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    static SecureRandom rnd = new SecureRandom();

    public static String getIdWithLeftPad(String id) {
        return StringUtils.leftPad(id, 2, '0');
    }

    public static String getIdWithLeftPad(Long id) {
        return getIdWithLeftPad(Objects.toString(id, ""));
    }

    public static String getStringWithZeroLeftPad(String id,Integer size) {
        return StringUtils.leftPad(id, size, '0');
    }

    public static String getStringWithZeroLeftPad(int id,Integer size) {
        return getStringWithZeroLeftPad(Objects.toString(id, ""),size);
    }

    public static Boolean isTimeBetweenTwoTime(String initialTime, String finalTime, String currentTime){
        try {
            String reg = "^([0-1][0-9]|2[0-3]):([0-5][0-9])$";/*:([0-5][0-9])*/
            if (initialTime.matches(reg) && finalTime.matches(reg) && currentTime.matches(reg)) {
                boolean valid = false;


                //Start Time
                Date inTime = new SimpleDateFormat("HH:mm").parse(initialTime);
                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(inTime);

                //Current Time
                Date checkTime = new SimpleDateFormat("HH:mm").parse(currentTime);
                Calendar calendar3 = Calendar.getInstance();
                calendar3.setTime(checkTime);

                //End Time
                Date finTime = new SimpleDateFormat("HH:mm").parse(finalTime);
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(finTime);

                if (finalTime.compareTo(initialTime) < 0) {
                    calendar2.add(Calendar.DATE, 1);
                    //calendar3.add(Calendar.DATE, 1);
                }
                System.out.println(calendar2.getTime());
                System.out.println(calendar3.getTime());

                Date actualTime = calendar3.getTime();
                if ((actualTime.after(calendar1.getTime()) || actualTime.compareTo(calendar1.getTime()) == 0)
                        && actualTime.before(calendar2.getTime())) {
                    valid = true;
                }

                return valid;
            } else {
                throw new IllegalArgumentException("Not a valid time, expecting HH:MM:SS format");
            }
        }catch (ParseException e){
            e.printStackTrace();
            System.out.println("Parse Exception");
            throw new IllegalArgumentException("Parse Exception");
        }catch (Exception e){
            e.printStackTrace();
            throw new IllegalArgumentException("Exception");
        }
    }

    public static String randomString(int len){
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ )
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

    public static String hashingMd5(String password){

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        md.update(password.getBytes());

        byte byteData[] = md.digest();

        //convert the byte to hex format method 1
        /*StringBuffer sb = new StringBuffer();
        for (int i = 0; i < byteData.length; i++) {
            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
        }*/

        //convert the byte to hex format method 2
        StringBuffer hexString = new StringBuffer();
        for (int i=0;i<byteData.length;i++) {
            String hex=Integer.toHexString(0xff & byteData[i]);
            if(hex.length()==1) hexString.append('0');
            hexString.append(hex);
        }

        return hexString.toString();
    }

    public static Timestamp getDateWithRemoveTime(String date){
        try {
            Date newFormateDate =  df.parse(date);
            Timestamp maxTimeDate = Timestamp.valueOf(newFormateDate+" "+"00:00:00.000");

            return maxTimeDate;
        }catch (ParseException pe){
            pe.printStackTrace();
            return null;
        }
    }

    public static Timestamp getDateWithMaxTime(String date){
        try {
            Date newFormateDate =  df.parse(date);
            Timestamp minTimeDate = Timestamp.valueOf(newFormateDate+" "+"23:59:59.999");

            return minTimeDate;
        }catch (ParseException pe){
            pe.printStackTrace();
            return null;
        }
    }

    public static Date getDateNextDay(String date){
        try {
            Calendar cal = Calendar.getInstance();
            Date newFormateDate =  df.parse(date);
            cal.setTime(newFormateDate);
            cal.add(Calendar.DATE, 1);
        return cal.getTime();
        }catch (ParseException pe){
            pe.printStackTrace();
            return null;
        }
    }
    public static String getCurrentDate(){
        Timestamp today = null;
        Date nowDate = Calendar.getInstance().getTime();
        today = new java.sql.Timestamp(nowDate.getTime());
       return today.toString().substring(0,10);
    }

    public static Timestamp getCurrentDateTimestamp() {
        Timestamp today = null;
        try {
            Date nowDate = Calendar.getInstance().getTime();
            today = new java.sql.Timestamp(nowDate.getTime());
        } catch (Exception e) {
            LOGGER.error("error msg : {} ", e);
            throw new RuntimeException(e);
        }
        return today;
    }

    public static Date getDate(String date){
        try {
            Date newFormateDate =  df.parse(date);
            return newFormateDate;
        }catch (ParseException pe){
            pe.printStackTrace();
            return null;
        }
    }

    public static Date newDateRemoveTime(){
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        LOGGER.debug("Date : {}",cal.getTime());
        return cal.getTime();
    }

    public static Date newDateSentPatameterRemoveTime(int date){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        LOGGER.debug("Date : {}",cal.getTime());
        return cal.getTime();
    }

    public static Date newDateRemoveTimeSentPatarameterLongDate(Long date){
        Calendar cal = Calendar.getInstance();
        Date newDate = new Date(date);
        cal.setTime(newDate);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date newDateSentPatameterRemoveTimeAndLongDate(Long date, int day){
        Calendar cal = Calendar.getInstance();
        Date newDate = new Date(date);
        cal.setTime(newDate);
        cal.add(Calendar.DATE, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }

    public static Date getEndOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        LOGGER.debug("Date : {}",calendar.getTime());
        return calendar.getTime();
    }

    public static Date getStartOfDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        LOGGER.debug("Date : {}",calendar.getTime());
        return calendar.getTime();
    }

    public static boolean isEmpty(String checkString){
        Boolean returnValue = true;
        if(checkString!=null && !checkString.isEmpty() && checkString.length()>0){
            returnValue = false;
        }
        return returnValue;
    }

    public static Date newDateSentPatameter(int date){
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, date);
        LOGGER.debug("Date : {}",cal.getTime());
        return cal.getTime();
    }

    public static String appendString(String oldText, String[] appendText){
        String newText = oldText;
        String[] array = appendText;
        for (String obj : array) {
            if (CommonUtil.isEmpty(newText)) {
                newText = obj;
            } else if (!newText.contains(obj)) {
                newText += "," + obj;
            }
        }

        return newText;
    }




    public static boolean isNotNull(Object o){
        if(o != null)
            return true;
        else
            return false;
    }


    public static String removeEnumber(String str) {



        //            }
                    Double number = Double.valueOf(str);
                    BigDecimal big = new BigDecimal(number);
        
                    return big.toString();
        
            }
}
