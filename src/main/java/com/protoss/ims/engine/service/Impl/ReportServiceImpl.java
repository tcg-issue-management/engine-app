package com.protoss.ims.engine.service.Impl;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import com.protoss.ims.engine.model.Cards;
import com.protoss.ims.engine.model.GroupLine;
import com.protoss.ims.engine.model.Project;
import com.protoss.ims.engine.model.ProjectGroup;
import com.protoss.ims.engine.model.UserLine;
import com.protoss.ims.engine.repository.CardsRepository;
import com.protoss.ims.engine.repository.GroupLineRepository;
import com.protoss.ims.engine.repository.ProjectGroupRepository;
import com.protoss.ims.engine.repository.ProjectRepository;
import com.protoss.ims.engine.repository.UserLineRepository;
import com.protoss.ims.engine.service.ProjectGroupService;
import com.protoss.ims.engine.service.ReportService;
import com.protoss.ims.engine.util.CommonUtil;
import org.springframework.beans.factory.annotation.Value;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.apache.poi.ss.util.CellRangeAddress;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.CellStyle;
import org.springframework.core.io.Resource;

@Service
public class ReportServiceImpl implements ReportService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    ProjectGroupRepository projectGroupRepository;

    @Autowired
    private CardsRepository cardsRepository;

    @Autowired
    UserLineRepository userLineRepository;

    @Value("classpath:/template_report/Template_Excel_IMS.xlsx")
    private Resource res;

    @PersistenceContext
    private EntityManager em;

    @Override
    public XSSFWorkbook generateReport(Long id,String startDate,String endDate, String status) {

        try {
           
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            InputStream resource = res.getInputStream();          
            XSSFWorkbook wb = new XSSFWorkbook(resource);

            XSSFDataFormat format = wb.createDataFormat();
            XSSFFont fontBold = wb.createFont();
            fontBold.setFontHeightInPoints((short) 10);
            fontBold.setFontName("ARIAL");
            fontBold.setBold(true);
            fontBold.setColor(new XSSFColor(new java.awt.Color(255, 0, 9)));

            XSSFFont fontBold_black_24 = wb.createFont();
            fontBold_black_24.setFontHeightInPoints((short) 24);
            fontBold_black_24.setFontName("ARIAL");
            fontBold_black_24.setBold(true);
            fontBold_black_24.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));

            XSSFFont fontBold_black_15 = wb.createFont();
            fontBold_black_15.setFontHeightInPoints((short) 15);
            fontBold_black_15.setFontName("ARIAL");
            fontBold_black_15.setBold(true);
            fontBold_black_15.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));

            XSSFFont fontBold_black_14 = wb.createFont();
            fontBold_black_14.setFontHeightInPoints((short) 14);
            fontBold_black_14.setFontName("ARIAL");
            fontBold_black_14.setBold(true);
            fontBold_black_14.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));

            XSSFFont fontNotBold_black_12 = wb.createFont();
            fontNotBold_black_12.setFontHeightInPoints((short) 12);
            fontNotBold_black_12.setFontName("ARIAL");

            XSSFFont fontBold_black_12 = wb.createFont();
            fontBold_black_12.setFontHeightInPoints((short) 12);
            fontBold_black_12.setFontName("ARIAL");
            fontBold_black_12.setBold(true);
            fontBold_black_12.setColor(new XSSFColor(new java.awt.Color(255, 255, 255)));

            XSSFFont fontNotBold8 = wb.createFont();
            fontNotBold8.setFontHeightInPoints((short) 8);
            fontNotBold8.setFontName("ARIAL");

            XSSFCellStyle styleReportName = wb.createCellStyle();
            styleReportName.setFont(fontBold_black_24);
            styleReportName.setAlignment(CellStyle.ALIGN_CENTER);
            styleReportName.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            styleReportName.setBorderTop(XSSFCellStyle.BORDER_THIN);
            styleReportName.setBorderRight(XSSFCellStyle.BORDER_THIN);
            styleReportName.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            styleReportName.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            styleReportName.setFillForegroundColor(new XSSFColor(new java.awt.Color(207, 226, 243)));
            styleReportName.setFillBackgroundColor(new XSSFColor(new java.awt.Color(207, 226, 243)));
            styleReportName.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle stylesub_ReportName = wb.createCellStyle();
            stylesub_ReportName.setFont(fontBold_black_15);
            stylesub_ReportName.setAlignment(CellStyle.ALIGN_LEFT);
            stylesub_ReportName.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            stylesub_ReportName.setBorderTop(XSSFCellStyle.BORDER_THIN);
            stylesub_ReportName.setBorderRight(XSSFCellStyle.BORDER_THIN);
            stylesub_ReportName.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            stylesub_ReportName.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            stylesub_ReportName.setFillForegroundColor(new XSSFColor(new java.awt.Color(207, 226, 243)));
            stylesub_ReportName.setFillBackgroundColor(new XSSFColor(new java.awt.Color(207, 226, 243)));
            stylesub_ReportName.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle stylesub_status_fillter = wb.createCellStyle();
            stylesub_status_fillter.setFont(fontBold_black_15);
            stylesub_status_fillter.setAlignment(CellStyle.ALIGN_LEFT);
            stylesub_status_fillter.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            stylesub_status_fillter.setBorderTop(XSSFCellStyle.BORDER_THIN);
            stylesub_status_fillter.setBorderRight(XSSFCellStyle.BORDER_THIN);
            stylesub_status_fillter.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            stylesub_status_fillter.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            stylesub_status_fillter.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            stylesub_status_fillter.setFillBackgroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            stylesub_status_fillter.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle styleHeader = wb.createCellStyle();
            styleHeader.setFont(fontBold_black_14);
            styleHeader.setAlignment(CellStyle.ALIGN_CENTER);
            styleHeader.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            styleHeader.setBorderTop(XSSFCellStyle.BORDER_THIN);
            styleHeader.setBorderRight(XSSFCellStyle.BORDER_THIN);
            styleHeader.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            styleHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            styleHeader.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            styleHeader.setFillBackgroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            styleHeader.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle styleHeader_issue_fillter = wb.createCellStyle();
            styleHeader_issue_fillter.setFont(fontNotBold_black_12);
            styleHeader_issue_fillter.setAlignment(CellStyle.ALIGN_LEFT);
            styleHeader_issue_fillter.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            styleHeader_issue_fillter.setBorderTop(XSSFCellStyle.BORDER_THIN);
            styleHeader_issue_fillter.setBorderRight(XSSFCellStyle.BORDER_THIN);
            styleHeader_issue_fillter.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            styleHeader_issue_fillter.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            styleHeader_issue_fillter.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            styleHeader_issue_fillter.setFillBackgroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            styleHeader_issue_fillter.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle styleSum_issue_fillter = wb.createCellStyle();
            styleSum_issue_fillter.setFont(fontBold_black_12);
            styleSum_issue_fillter.setAlignment(CellStyle.ALIGN_RIGHT);
            styleSum_issue_fillter.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            styleSum_issue_fillter.setBorderTop(XSSFCellStyle.BORDER_THIN);
            styleSum_issue_fillter.setBorderRight(XSSFCellStyle.BORDER_THIN);
            styleSum_issue_fillter.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            styleSum_issue_fillter.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            styleSum_issue_fillter.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            styleSum_issue_fillter.setFillBackgroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            styleSum_issue_fillter.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle styleDetail_align_center = wb.createCellStyle();
            styleDetail_align_center.setFont(fontNotBold_black_12);
            styleDetail_align_center.setAlignment(CellStyle.ALIGN_CENTER);
            styleDetail_align_center.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            styleDetail_align_center.setBorderTop(XSSFCellStyle.BORDER_THIN);
            styleDetail_align_center.setBorderRight(XSSFCellStyle.BORDER_THIN);
            styleDetail_align_center.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            styleDetail_align_center.setWrapText(true);
            styleDetail_align_center.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle styleDetail_align_left = wb.createCellStyle();
            styleDetail_align_left.setFont(fontNotBold_black_12);
            styleDetail_align_left.setAlignment(CellStyle.ALIGN_LEFT);
            styleDetail_align_left.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            styleDetail_align_left.setBorderTop(XSSFCellStyle.BORDER_THIN);
            styleDetail_align_left.setBorderRight(XSSFCellStyle.BORDER_THIN);
            styleDetail_align_left.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            styleDetail_align_left.setWrapText(true);
            styleDetail_align_left.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle styleDetail_align_right = wb.createCellStyle();
            styleDetail_align_right.setFont(fontNotBold_black_12);
            styleDetail_align_right.setAlignment(CellStyle.ALIGN_RIGHT);
            styleDetail_align_right.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            styleDetail_align_right.setBorderTop(XSSFCellStyle.BORDER_THIN);
            styleDetail_align_right.setBorderRight(XSSFCellStyle.BORDER_THIN);
            styleDetail_align_right.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            styleDetail_align_right.setWrapText(true);
            styleDetail_align_right.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle style_sum_total = wb.createCellStyle();
            style_sum_total.setFont(fontBold_black_14);
            style_sum_total.setAlignment(CellStyle.ALIGN_RIGHT);
            style_sum_total.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            style_sum_total.setBorderTop(XSSFCellStyle.BORDER_THIN);
            style_sum_total.setBorderRight(XSSFCellStyle.BORDER_THIN);
            style_sum_total.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            style_sum_total.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            style_sum_total.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            style_sum_total.setFillBackgroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            style_sum_total.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle style_sum_total_cell = wb.createCellStyle();
            style_sum_total_cell.setFont(fontNotBold_black_12);
            style_sum_total_cell.setAlignment(CellStyle.ALIGN_RIGHT);
            style_sum_total_cell.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            style_sum_total_cell.setBorderTop(XSSFCellStyle.BORDER_THIN);
            style_sum_total_cell.setBorderRight(XSSFCellStyle.BORDER_THIN);
            style_sum_total_cell.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            style_sum_total_cell.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            style_sum_total_cell.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            style_sum_total_cell.setFillBackgroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            style_sum_total_cell.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle style_sum_total_all = wb.createCellStyle();
            style_sum_total_all.setFont(fontBold_black_14);
            style_sum_total_all.setAlignment(CellStyle.ALIGN_CENTER);
            style_sum_total_all.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            style_sum_total_all.setBorderTop(XSSFCellStyle.BORDER_THIN);
            style_sum_total_all.setBorderRight(XSSFCellStyle.BORDER_THIN);
            style_sum_total_all.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            style_sum_total_all.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            style_sum_total_all.setFillForegroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            style_sum_total_all.setFillBackgroundColor(new XSSFColor(new java.awt.Color(255, 242, 204)));
            style_sum_total_all.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle style_status_fillter_open_desc = wb.createCellStyle();
            style_status_fillter_open_desc.setFont(fontNotBold_black_12);
            style_status_fillter_open_desc.setAlignment(CellStyle.ALIGN_LEFT);
            style_status_fillter_open_desc.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_open_desc.setBorderTop(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_open_desc.setBorderRight(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_open_desc.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_open_desc.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            style_status_fillter_open_desc.setFillForegroundColor(new XSSFColor(new java.awt.Color(204, 204, 204)));
            style_status_fillter_open_desc.setFillBackgroundColor(new XSSFColor(new java.awt.Color(204, 204, 204)));
            style_status_fillter_open_desc.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle style_status_fillter_open_value = wb.createCellStyle();
            style_status_fillter_open_value.setFont(fontNotBold_black_12);
            style_status_fillter_open_value.setAlignment(CellStyle.ALIGN_RIGHT);
            style_status_fillter_open_value.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_open_value.setBorderTop(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_open_value.setBorderRight(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_open_value.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_open_value.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            style_status_fillter_open_value.setFillForegroundColor(new XSSFColor(new java.awt.Color(204, 204, 204)));
            style_status_fillter_open_value.setFillBackgroundColor(new XSSFColor(new java.awt.Color(204, 204, 204)));
            style_status_fillter_open_value.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle style_status_fillter_assign_desc = wb.createCellStyle();
            style_status_fillter_assign_desc.setFont(fontNotBold_black_12);
            style_status_fillter_assign_desc.setAlignment(CellStyle.ALIGN_LEFT);
            style_status_fillter_assign_desc.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_assign_desc.setBorderTop(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_assign_desc.setBorderRight(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_assign_desc.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_assign_desc.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            style_status_fillter_assign_desc.setFillForegroundColor(new XSSFColor(new java.awt.Color(244, 204, 204)));
            style_status_fillter_assign_desc.setFillBackgroundColor(new XSSFColor(new java.awt.Color(244, 204, 204)));
            style_status_fillter_assign_desc.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle style_status_fillter_assign_value = wb.createCellStyle();
            style_status_fillter_assign_value.setFont(fontNotBold_black_12);
            style_status_fillter_assign_value.setAlignment(CellStyle.ALIGN_RIGHT);
            style_status_fillter_assign_value.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_assign_value.setBorderTop(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_assign_value.setBorderRight(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_assign_value.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_assign_value.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            style_status_fillter_assign_value.setFillForegroundColor(new XSSFColor(new java.awt.Color(244, 204, 204)));
            style_status_fillter_assign_value.setFillBackgroundColor(new XSSFColor(new java.awt.Color(244, 204, 204)));
            style_status_fillter_assign_value.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle style_status_fillter_close_desc = wb.createCellStyle();
            style_status_fillter_close_desc.setFont(fontNotBold_black_12);
            style_status_fillter_close_desc.setAlignment(CellStyle.ALIGN_LEFT);
            style_status_fillter_close_desc.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_close_desc.setBorderTop(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_close_desc.setBorderRight(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_close_desc.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_close_desc.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            style_status_fillter_close_desc.setFillForegroundColor(new XSSFColor(new java.awt.Color(182, 215, 168)));
            style_status_fillter_close_desc.setFillBackgroundColor(new XSSFColor(new java.awt.Color(182, 215, 168)));
            style_status_fillter_close_desc.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            XSSFCellStyle style_status_fillter_close_value = wb.createCellStyle();
            style_status_fillter_close_value.setFont(fontNotBold_black_12);
            style_status_fillter_close_value.setAlignment(CellStyle.ALIGN_RIGHT);
            style_status_fillter_close_value.setBorderBottom(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_close_value.setBorderTop(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_close_value.setBorderRight(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_close_value.setBorderLeft(XSSFCellStyle.BORDER_THIN);
            style_status_fillter_close_value.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
            style_status_fillter_close_value.setFillForegroundColor(new XSSFColor(new java.awt.Color(182, 215, 168)));
            style_status_fillter_close_value.setFillBackgroundColor(new XSSFColor(new java.awt.Color(182, 215, 168)));
            style_status_fillter_close_value.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

            int sum_all_group_status_open = 0;
            int sum_all_group_status_assign = 0;
            int sum_all_group_status_close = 0;
            
            List<Map<String,Object>> listData = new ArrayList<>();
            Map<String,Object> mapObject = null;
            List<ProjectGroup> listProjectGroup = projectGroupRepository.findProjectGroupByProjectId(id);
            XSSFSheet sheet;
            if (listProjectGroup.size() != 0 && listProjectGroup != null) {
                for (ProjectGroup p : listProjectGroup) {
                    mapObject = new HashMap<>();
                    mapObject.put("name",p.getDisplayName());
                    sheet = wb.createSheet(p.getDisplayName());
                    sheet.addMergedRegion(new CellRangeAddress(1, 2, 1, 3));
                    sheet.addMergedRegion(new CellRangeAddress(3, 4, 1, 3));
                    sheet.addMergedRegion(new CellRangeAddress(5, 6, 1, 3));
                    XSSFRow nameReportRow = sheet.createRow(1);
                    XSSFCell nameReportDesc = nameReportRow.createCell(2);
                    nameReportDesc.setCellValue("Summary Issue Report");
                    nameReportDesc.setCellStyle(styleReportName);

                    XSSFRow groupRow = sheet.createRow(3);
                    XSSFCell groupNameCell = groupRow.createCell(2);
                    groupNameCell.setCellValue("Group : " + p.getDisplayName());
                    groupNameCell.setCellStyle(stylesub_ReportName);

                    XSSFRow dateExportRow = sheet.createRow(5);
                    XSSFCell dateCell = dateExportRow.createCell(2);
                    dateCell.setCellValue("Date Export : " + CommonUtil.getCurrentDate());
                    dateCell.setCellStyle(stylesub_ReportName);

                    sheet.addMergedRegion(new CellRangeAddress(8, 8, 1, 2));
                    XSSFRow issue_status_sum = sheet.createRow(8);
                    XSSFCell status_list_cell = issue_status_sum.createCell(2);
                    status_list_cell.setCellValue("Status List");
                    status_list_cell.setCellStyle(stylesub_status_fillter);

                    XSSFRow issue_status_open_row = sheet.createRow(9);
                    XSSFCell issue_status_open_cell_desc = issue_status_open_row.createCell(1);
                    issue_status_open_cell_desc.setCellValue("Open");
                    issue_status_open_cell_desc.setCellStyle(style_status_fillter_open_desc);
                    XSSFCell issue_status_open_cell_value = issue_status_open_row.createCell(2);
                    issue_status_open_cell_value.setCellStyle(style_status_fillter_open_value);

                    XSSFRow issue_status_assign_row = sheet.createRow(10);
                    XSSFCell issue_status_assign_cell_desc = issue_status_assign_row.createCell(1);
                    issue_status_assign_cell_desc.setCellValue("Assign");
                    issue_status_assign_cell_desc.setCellStyle(style_status_fillter_assign_desc);
                    XSSFCell issue_status_assign_cell_value = issue_status_assign_row.createCell(2);
                    issue_status_assign_cell_value.setCellStyle(style_status_fillter_assign_value);

                    XSSFRow issue_status_close_row = sheet.createRow(11);
                    XSSFCell issue_status_close_cell_desc = issue_status_close_row.createCell(1);
                    issue_status_close_cell_desc.setCellValue("Close");
                    issue_status_close_cell_desc.setCellStyle(style_status_fillter_close_desc);
                    XSSFCell issue_status_close_cell_value = issue_status_close_row.createCell(2);
                    issue_status_close_cell_value.setCellStyle(style_status_fillter_close_value);

                    sheet.addMergedRegion(new CellRangeAddress(12, 12, 1, 2));
                    XSSFRow issue_fillter_status_row = sheet.createRow(12);
                    XSSFCell issue_fillter_status_cell = issue_fillter_status_row.createCell(1);
                    issue_fillter_status_cell.setCellStyle(styleSum_issue_fillter);

                    int count_status_open = 0;
                    int count_status_assign = 0;
                    int count_status_close = 0;
                    int sum_count_status = 0;

                    XSSFRow headerRow = sheet.createRow(14);
                    XSSFCell headerCell_issueNo = headerRow.createCell(1);
                    headerCell_issueNo.setCellValue("Issue No");
                    headerCell_issueNo.setCellStyle(styleHeader);

                    XSSFCell headerCell_detail = headerRow.createCell(2);
                    headerCell_detail.setCellValue("Detail");
                    headerCell_detail.setCellStyle(styleHeader);

                    XSSFCell headerCell_issueDate = headerRow.createCell(3);
                    headerCell_issueDate.setCellValue("Issue Date");
                    headerCell_issueDate.setCellStyle(styleHeader);

                    XSSFCell headerCell_sender = headerRow.createCell(4);
                    headerCell_sender.setCellValue("Sender");
                    headerCell_sender.setCellStyle(styleHeader);

                    XSSFCell headerCell_status = headerRow.createCell(5);
                    headerCell_status.setCellValue("Status");
                    headerCell_status.setCellStyle(styleHeader);

                    XSSFCell detailCell_issueNo;
                    XSSFCell detailCell_detail;
                    XSSFCell detailCell_issueDate;
                    XSSFCell detailCell_sender;
                    XSSFCell detailCell_status;

                    /* ERROR POINT */
                    Criteria criteria = ((Session) em.getDelegate()).createCriteria(Cards.class, "Cards");
                    criteria.createAlias("Cards.projectGroup", "projectGroup");
                    
                    criteria.add(Restrictions.in("projectGroup.id", p.getId()));

                    if(!"All".equals(status)){
                        criteria.add(Restrictions.eq("Cards.status", status));
                    }
                    if(!"".equals(startDate)){
                        Long startDate_convert = Long.valueOf(startDate);
                        criteria.add(Restrictions.ge("Cards.createdDate", new Date(startDate_convert))); 
                    }
                    if(!"".equals(endDate)){
                        Long endDate_convert = Long.valueOf(endDate);
                        criteria.add(Restrictions.lt("Cards.createdDate", new Date(endDate_convert))); 
                    }
                    List<Cards> listCard =  criteria.list();
                    // List<Cards> listCard = cardsRepository.findCardsByProjectGroup(p.getId());
                    
                    int count_detail_row = 14;
                    if (listCard.size() != 0) {
                        for (Cards card : listCard) {
                            count_detail_row++;
                            XSSFRow detailRow = sheet.createRow(count_detail_row);

                            detailCell_issueNo = detailRow.createCell(1);
                            detailCell_detail = detailRow.createCell(2);
                            detailCell_issueDate = detailRow.createCell(3);
                            detailCell_sender = detailRow.createCell(4);
                            detailCell_status = detailRow.createCell(5);

                            detailCell_issueNo.setCellValue(card.getId().toString());
                            detailCell_detail.setCellValue(card.getMsg());
                            // Date date_convert = CommonUtil.getDate(new
                            // Date(card.getCreatedDate().getTime()).toString());
                            // String dateNumber =
                            // CommonUtil.removeEnumber(card.getCreatedDate().toString());
                            detailCell_issueDate.setCellValue(card.getCreatedDate().toString().substring(0, 10));
                            UserLine userLine = userLineRepository.findByRealIdEquals(card.getSender());
                            detailCell_sender.setCellValue(userLine != null ? userLine.getDisplayName() : "");
                            detailCell_status.setCellValue(card.getStatus());

                            detailCell_issueNo.setCellStyle(styleDetail_align_center);
                            detailCell_detail.setCellStyle(styleDetail_align_left);
                            detailCell_issueDate.setCellStyle(styleDetail_align_center);
                            detailCell_sender.setCellStyle(styleDetail_align_left);
                            detailCell_status.setCellStyle(styleDetail_align_center);

                            if (card.getStatus() != null) {
                                switch (card.getStatus()) {
                                case "open":
                                    count_status_open++;
                                    sum_all_group_status_open++;
                                    break;
                                case "assign":
                                    count_status_assign++;
                                    sum_all_group_status_assign++;
                                    break;
                                case "close":
                                    count_status_close++;
                                    sum_all_group_status_close++;
                                    break;
                                default:
                                    break;
                                }
                            }

                        }

                        sum_count_status = count_status_open + count_status_assign + count_status_close;
                        sheet.getRow(9).getCell(2).setCellValue(count_status_open);
                        sheet.getRow(10).getCell(2).setCellValue(count_status_assign);
                        sheet.getRow(11).getCell(2).setCellValue(count_status_close);
                        sheet.getRow(12).getCell(1).setCellValue("Total = " + sum_count_status);

                    }

                    mapObject.put("open",count_status_open);
                    mapObject.put("assign",count_status_assign);
                    mapObject.put("close",count_status_close);
                    listData.add(mapObject);

                    sheet.setColumnWidth(1, 4500);
                    sheet.setColumnWidth(2, 10000);
                    sheet.setColumnWidth(3, 5000);
                    sheet.setColumnWidth(4, 5000);
                    sheet.setColumnWidth(5, 3500);

                }

                sheet = wb.getSheet("Summary All");
                int summary_count_row_all = 6;
                for(Map<String,Object> map : listData){
                    XSSFRow row_summary = sheet.createRow(summary_count_row_all);
                    XSSFCell cell_summary_group_name = row_summary.createCell(4);
                    cell_summary_group_name.setCellValue(String.valueOf(map.get("name")));
                    cell_summary_group_name.setCellStyle(styleDetail_align_left);
                    XSSFCell cell_summary_status_open = row_summary.createCell(5);
                    cell_summary_status_open.setCellValue(String.valueOf(map.get("open")));
                    cell_summary_status_open.setCellStyle(styleDetail_align_right);
                    XSSFCell cell_summary_status_assign = row_summary.createCell(6);
                    cell_summary_status_assign.setCellValue(String.valueOf(map.get("assign")));
                    cell_summary_status_assign.setCellStyle(styleDetail_align_right);
                    XSSFCell cell_summary_status_close = row_summary.createCell(7);
                    cell_summary_status_close.setCellValue(String.valueOf(map.get("close")));
                    cell_summary_status_close.setCellStyle(styleDetail_align_right);
                    summary_count_row_all++;
                }
                XSSFRow row_summary_all = sheet.createRow(summary_count_row_all);
                XSSFCell totalCell = row_summary_all.createCell(4);
                totalCell.setCellValue("Total");
                totalCell.setCellStyle(style_sum_total_all);
                XSSFCell total_status_open = row_summary_all.createCell(5);
                total_status_open.setCellValue(sum_all_group_status_open);
                total_status_open.setCellStyle(style_sum_total_cell);
                XSSFCell total_status_assign = row_summary_all.createCell(6);
                total_status_assign.setCellValue(sum_all_group_status_assign);
                total_status_assign.setCellStyle(style_sum_total_cell);
                XSSFCell total_status_close = row_summary_all.createCell(7);
                total_status_close.setCellValue(sum_all_group_status_close);
                total_status_close.setCellStyle(style_sum_total_cell);
            }

            return wb;

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public XSSFWorkbook generateReportWithTemplate()  {
        

        XSSFWorkbook workbook = null;
        try{
            InputStream resource = res.getInputStream();          
            workbook = new XSSFWorkbook(resource);
            return workbook;
    
        }catch(FileNotFoundException fe){
            return workbook;
        }catch(IOException fe){
            return workbook;
        }
    }


 
}
