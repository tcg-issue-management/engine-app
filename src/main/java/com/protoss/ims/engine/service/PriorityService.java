package com.protoss.ims.engine.service;

import org.springframework.stereotype.Service;

import com.protoss.ims.engine.model.Priority;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Ratthanan W.
 */
@Service
public interface PriorityService {

	List<Priority> findAllPriorityLike(String hasText,
									   String flagActive,
									   String responseValue,
									   int firstResult,
									   int maxResult);

	Long  findAllPriorityLikeSize(String hasText,
								  String flagActive,
								  String responseValue
								 );

	public void persist(Priority priority, HttpServletRequest httpServletRequest);


}
