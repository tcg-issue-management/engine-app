package com.protoss.ims.engine.service;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public interface ReportService {


	XSSFWorkbook generateReport(Long id,String startDate,String endDate, String status);
	XSSFWorkbook generateReportWithTemplate();


}
