package com.protoss.ims.engine.service.Impl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.protoss.ims.engine.model.Priority;
import com.protoss.ims.engine.model.Project;
import com.protoss.ims.engine.repository.PriorityRepository;
import com.protoss.ims.engine.repository.ProjectRepository;
import com.protoss.ims.engine.service.PriorityService;
import com.protoss.ims.engine.service.ProjectService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;


public class ProjectServiceImpl implements ProjectService {
    private final static Logger LOGGER = LoggerFactory.getLogger(ProjectServiceImpl.class);


    @PersistenceContext
    private EntityManager em;

    @Autowired
    ProjectRepository projectRepository;



    @Override
    public List<Project> findAllProject() {
        return projectRepository.findAll();
    }


    @Transactional
    @Override
    public void saveProject(String projectName) {
        try{
            Project project = new Project();
            project.setProjectName(projectName);
            projectRepository.saveAndFlush(project);
        }catch (Exception e){
            e.printStackTrace();
            throw new RuntimeException("saveProject exception");
        }
    }
}

