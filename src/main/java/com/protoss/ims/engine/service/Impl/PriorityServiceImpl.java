package com.protoss.ims.engine.service.Impl;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.protoss.ims.engine.model.Priority;
import com.protoss.ims.engine.repository.PriorityRepository;
import com.protoss.ims.engine.service.PriorityService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.List;

public class PriorityServiceImpl implements PriorityService {
    private final static Logger LOGGER = LoggerFactory.getLogger(PriorityServiceImpl.class);


    @PersistenceContext
    private EntityManager em;

    @Autowired
    PriorityRepository priorityRepository;


    @Override
    public List<Priority> findAllPriorityLike(String hasText,
                                              String flagActive,
                                              String responseValue,
                                              int firstResult,
                                              int maxResult) {
        LOGGER.debug(" ===- In findAllPriorityLike -=== ");

        try {

            Criteria criteria = ((Session) em.getDelegate()).createCriteria(Priority.class, "Priority").setFirstResult(firstResult).setMaxResults(maxResult);
            criteria.add(Restrictions.like("Priority.hasText", "%" + hasText + "%"));
            criteria.add(Restrictions.like("Priority.responseValue", "%" + responseValue + "%"));
            criteria.add(Restrictions.eq("Priority.flagActive", flagActive));

//        criteria.addOrder(Order.desc("Register.createDate"));
            criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            LOGGER.debug(" ===- Out findAllPriorityLike -=== ");
            return criteria.list();
        } catch (Exception e) {
            LOGGER.error(" ===- Exception[findAllPriorityLike] Msg : [{}] -=== ", e.getMessage());
            e.printStackTrace();
            return null;
        }

    }

    @Override
    public Long findAllPriorityLikeSize(String hasText,
                                        String flagActive,
                                        String responseValue) {
        Long count = 0l;
        try {

            Criteria criteria = ((Session) em.getDelegate()).createCriteria(Priority.class, "Priority");
            criteria.add(Restrictions.like("Priority.hasText", "%" + hasText + "%"));
            criteria.add(Restrictions.like("Priority.responseValue", "%" + responseValue + "%"));
            criteria.add(Restrictions.eq("Priority.flagActive", flagActive));

//        criteria.addOrder(Order.desc("Register.createDate"));
            criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            LOGGER.debug(" ===- Out findAllPriorityLikeSize -=== ");
            count = Long.valueOf(criteria.list().size());
            return count;

        } catch (Exception e) {
            LOGGER.error(" ===- Exception[findAllPriorityLikeSize] Msg : [{}] -=== ", e.getMessage());
            e.printStackTrace();
            return -1l;
        }

    }

    @Transactional
    @Override
    public void persist(Priority priority, HttpServletRequest httpServletRequest) {

        try {
            priorityRepository.saveAndFlush(priority);
        } catch (Exception e) {
            LOGGER.error("Exception[persist] Msg : [{}] ", e.getMessage());
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

}

