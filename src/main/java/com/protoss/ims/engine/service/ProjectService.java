package com.protoss.ims.engine.service;

import org.springframework.http.ResponseEntity;

import com.protoss.ims.engine.model.Project;

import java.util.List;

public interface ProjectService {



    List<Project> findAllProject();
    void saveProject(String projectName);

}
