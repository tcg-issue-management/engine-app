package com.protoss.ims.engine.service.Impl;

import com.protoss.ims.engine.model.UserLine;
import com.protoss.ims.engine.repository.UserLineRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.protoss.ims.engine.model.Cards;
import com.protoss.ims.engine.model.ProjectGroup;
import com.protoss.ims.engine.repository.CardsRepository;
import com.protoss.ims.engine.repository.ProjectGroupRepository;
import com.protoss.ims.engine.service.CardsService;
import com.protoss.ims.engine.service.ProjectGroupService;
import com.protoss.ims.engine.util.CommonUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class CardsServiceImpl implements CardsService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CardsRepository cardsRepository;

    @Autowired
    ProjectGroupRepository projectGroupRepository;

    @Autowired
    UserLineRepository userLineRepository;


    @Override
    public List<Map<String, Object>> findCardList(Long id) {
        List<Map<String, Object>> listObj = new ArrayList<>();
        Map<String,Object> mapResult;

        List<ProjectGroup> listProjectGroup = projectGroupRepository.findProjectGroupByProjectId(id);

            if(listProjectGroup.size() != 0 && listProjectGroup != null){
                for(ProjectGroup p : listProjectGroup){
                    mapResult = new HashMap<>();
                    mapResult.put("groupName",p.getDisplayName());
                    mapResult.put("codeProject",p.getCode());
                    mapResult.put("idProjectGroup",p.getId());
                    List<Cards> listCard = cardsRepository.findCardsByProjectGroup(p.getId());
                    mapResult.put("cardList",listCard);
                    for(Cards card : listCard){
                        UserLine ul = userLineRepository.findByRealIdEquals(card.getSender());
                        if(ul != null){
                            card.setSender(ul.getDisplayName()); 
                        }
                        
                    }
                    listObj.add(mapResult);
                }
            }

        return listObj;
    }

    @Override
    public List<Map<String, Object>> findCardsByProjectIdAndStatus(Long id, String status) {
        List<Map<String, Object>> listObj = new ArrayList<>();
        Map<String,Object> mapResult;

        List<ProjectGroup> listProjectGroup = projectGroupRepository.findProjectGroupByProjectId(id);

        if(listProjectGroup.size() != 0 && listProjectGroup != null){
            for(ProjectGroup p : listProjectGroup){
                mapResult = new HashMap<>();
                mapResult.put("groupName",p.getDisplayName());
                mapResult.put("codeProject",p.getCode());
                mapResult.put("idProjectGroup",p.getId());
                List<Cards> listCard = new ArrayList<>();
                if("all".equalsIgnoreCase(status)){
                    listCard = cardsRepository.findCardsByProjectGroupAndStatus(p.getId(),"");
                }else{
                    listCard = cardsRepository.findCardsByProjectGroupAndStatus(p.getId(),status);
                }

                if(listCard.size() > 0){
                    for (Cards c : listCard){
                        if(null != c.getSender()){
                            UserLine userLine = userLineRepository.findByRealIdEquals(c.getSender());
                            if(userLine != null){
                                c.setSender(userLine.getDisplayName());
                            }
                            
                        }
                    }
                }
                mapResult.put("cardList",listCard);
                listObj.add(mapResult);
            }
        }

        return listObj;
    }
    
    @Override
    public void saveCard(String json) {

        try{
            ObjectMapper mapper = new ObjectMapper();
            Map<String, String> map = mapper.readValue(json, Map.class);
            Cards objectSave = new Cards();
            Long idPg = Long.valueOf(String.valueOf(map.get("idProjectGroup")));
            LOGGER.info(" id project Group : [{}]",idPg);
            ProjectGroup projectGroup = projectGroupRepository.fProjectGroupsById(idPg).get(0);
            objectSave.setProjectGroup(projectGroup);
            objectSave.setCreatedDate(CommonUtil.getCurrentDateTimestamp());
            objectSave.setMsg(map.get("msg"));
            objectSave.setSender(map.get("sender"));
            objectSave.setStatus("open");
            cardsRepository.saveAndFlush(objectSave);
        }catch(IOException e){
                LOGGER.error("===- Error IOException Msg : [{}] -=== ",e.getMessage());
                throw new RuntimeException();
        }catch(Exception e ){
              LOGGER.error("===- Error Exception Msg : [{}] -=== ",e.getMessage());
              e.printStackTrace();
              throw new RuntimeException();
        }
   

    }
}
