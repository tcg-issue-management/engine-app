package com.protoss.ims.engine.service;

import java.util.List;

import com.protoss.ims.engine.model.ProjectGroup;

public interface ProjectGroupService {

    List<ProjectGroup> findProjectGroupAndCards(Long id);
    void saveProjectGroup(String code, Long projectId);

}
