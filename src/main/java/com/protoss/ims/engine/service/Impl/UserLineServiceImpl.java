package com.protoss.ims.engine.service.Impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.protoss.ims.engine.model.Cards;
import com.protoss.ims.engine.model.ProjectGroup;
import com.protoss.ims.engine.model.UserGroup;
import com.protoss.ims.engine.repository.CardsRepository;
import com.protoss.ims.engine.repository.ProjectGroupRepository;
import com.protoss.ims.engine.repository.UserGroupRepository;
import com.protoss.ims.engine.service.CardsService;
import com.protoss.ims.engine.service.UserLineService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserLineServiceImpl implements UserLineService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserGroupRepository userGroupRepository;

    @Autowired
    ProjectGroupRepository projectGroupRepository;


    @Override
    public List<Map<String, Object>> findUserLing(Long id) {
        List<Map<String, Object>> listObj = new ArrayList<>();
        Map<String,Object> mapResult;

        List<ProjectGroup> listProjectGroup = projectGroupRepository.findProjectGroupByProjectId(id);

            if(listProjectGroup.size() != 0 && listProjectGroup != null){
                for(ProjectGroup p : listProjectGroup){
                    mapResult = new HashMap<>();
                    mapResult.put("groupName",p.getDisplayName());
                    mapResult.put("codeProject",p.getCode());
                    List<UserGroup> userGroupList = userGroupRepository.findUserGroupByGroupLineId(p.getRealId());
                    mapResult.put("userList",userGroupList);
                    listObj.add(mapResult);
                }
            }

        return listObj;
    }
}
