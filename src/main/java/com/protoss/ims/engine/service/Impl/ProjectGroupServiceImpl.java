package com.protoss.ims.engine.service.Impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.protoss.ims.engine.model.GroupLine;
import com.protoss.ims.engine.model.Project;
import com.protoss.ims.engine.model.ProjectGroup;
import com.protoss.ims.engine.repository.GroupLineRepository;
import com.protoss.ims.engine.repository.ProjectGroupRepository;
import com.protoss.ims.engine.repository.ProjectRepository;
import com.protoss.ims.engine.service.ProjectGroupService;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProjectGroupServiceImpl implements ProjectGroupService {
    private Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ProjectGroupRepository projectGroupRepository;

    @Autowired
    private GroupLineRepository groupLineRepository;

    @Autowired
    ProjectRepository projectRepository;

    @Override
    public List<ProjectGroup> findProjectGroupAndCards(Long id) {
        return projectGroupRepository.findProjectGroupByProjectId(id);
    }

    @Transactional
    @Override
    public void saveProjectGroup(String code,Long projectId) {

        List<GroupLine> groupLines = groupLineRepository.findGroupLineByCode(code);
        if(groupLines != null ){
            GroupLine groupLineObj = groupLines.get(0);
            ProjectGroup projectGroup = new ProjectGroup();
            projectGroup.setCode(groupLineObj.getCode());
            projectGroup.setDisplayName(groupLineObj.getDisplayName());
            projectGroup.setRealId(groupLineObj.getRealId());

            List<Project> listProject = projectRepository.findProjectById(projectId);
            if(listProject != null)
                projectGroup.setProject(listProject.get(0));

            projectGroupRepository.saveAndFlush(projectGroup);

        }

    }

}
