package com.protoss.ims.engine.service;

import java.util.List;
import java.util.Map;

import com.protoss.ims.engine.model.Cards;
import com.protoss.ims.engine.model.ProjectGroup;

public interface CardsService {

   List<Map<String,Object>> findCardList(Long id);
   List<Map<String,Object>> findCardsByProjectIdAndStatus(Long id,String status);
   void saveCard(String json);
   
}
