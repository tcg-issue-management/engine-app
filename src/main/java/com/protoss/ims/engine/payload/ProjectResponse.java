package com.protoss.ims.engine.payload;

public class ProjectResponse {

    private Long id;
    private String createdBy;
    private String projectName;

    public ProjectResponse(Long id, String createdBy, String projectName) {
        this.id = id;
        this.createdBy = createdBy;
        this.projectName = projectName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }
}
