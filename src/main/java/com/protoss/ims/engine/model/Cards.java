package com.protoss.ims.engine.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of={"id"})
public class Cards {

    private @Id @GeneratedValue(strategy= GenerationType.TABLE) Long id;
    private @Version @JsonIgnore Long version;
    private String createdBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss") Timestamp createdDate;
    private String updateBy;
    private @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")Timestamp updateDate;

    private String msg;
    private Integer priority;  /* 0 = Critical, 1 = Error, 2 = Warning, 3 = Normal */
    private String groupLine;
    private String sender;
    private String status;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "projectGroup")
    private ProjectGroup projectGroup;

}
