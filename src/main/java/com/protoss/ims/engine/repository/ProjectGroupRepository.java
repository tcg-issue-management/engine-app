package com.protoss.ims.engine.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.protoss.ims.engine.model.ProjectGroup;

import java.util.List;

@Repository
public interface ProjectGroupRepository extends JpaRepository<ProjectGroup, Long> {
    @Query("select p from ProjectGroup p where p.project.id =:projectId and p.code <> null ")
    List<ProjectGroup> findProjectGroupByProjectId(@Param("projectId") Long id);

    @Query("select p from ProjectGroup p where p.id =:id ")
    List<ProjectGroup> fProjectGroupsById(@Param("id") Long id);

}
