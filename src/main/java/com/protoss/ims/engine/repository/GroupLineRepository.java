package com.protoss.ims.engine.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import com.protoss.ims.engine.model.Cards;
import com.protoss.ims.engine.model.GroupLine;

import java.util.List;

@Repository
public interface GroupLineRepository extends JpaRepository<GroupLine, Long> {



    @Query("select distinct g from GroupLine g " +
            "where g.code = :code " +
            "")
    List<GroupLine> findGroupLineByCode(@RequestParam("code") String code);
}
