package com.protoss.ims.engine.repository;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.protoss.ims.engine.model.Priority;
import com.protoss.ims.engine.util.CommonUtil;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.*;

/**
 * Created by Ratthanan
 */
public class PriorityRepositoryImpl {

    private final static Logger LOGGER = LoggerFactory.getLogger(PriorityRepositoryImpl.class);

    @PersistenceContext
    private EntityManager em;


    public List<Priority> findAllPriorityLike(String hasText,
                                              String flagActive,
                                              String responseValue,
                                              int firstResult,
                                              int maxResult) {
        LOGGER.debug(" ===- In findAllPriorityLike -=== ");

        try {

            Criteria criteria = ((Session) em.getDelegate()).createCriteria(Priority.class, "Priority").setFirstResult(firstResult).setMaxResults(maxResult);
            if (!CommonUtil.isEmpty(hasText))
                 criteria.add(Restrictions.like("Priority.hasText", "%" + hasText + "%"));
            if (!CommonUtil.isEmpty(responseValue))
                criteria.add(Restrictions.like("Priority.responseValue", "%" + responseValue + "%"));
            if (!CommonUtil.isEmpty(flagActive))
                criteria.add(Restrictions.eq("Priority.flagActive", flagActive));

//        criteria.addOrder(Order.desc("Register.createDate"));
            criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
            LOGGER.debug(" ===- Out findAllPriorityLike -=== ");
            return criteria.list();
        }catch (Exception e){
            LOGGER.error(" ===- Exception[findAllPriorityLike] Msg : [{}] -=== ",e.getMessage());
            e.printStackTrace();
            return null;
        }

    }


    public Long findAllPriorityLikeSize(String hasText,
                                        String flagActive,
                                        String responseValue) {
        Long count = 0l;
        try {

            Criteria criteria = ((Session) em.getDelegate()).createCriteria(Priority.class, "Priority");
            if (!CommonUtil.isEmpty(hasText))
                criteria.add(Restrictions.like("Priority.hasText", "%" + hasText + "%"));
            if (!CommonUtil.isEmpty(responseValue))
                criteria.add(Restrictions.like("Priority.responseValue", "%" + responseValue + "%"));
            if (!CommonUtil.isEmpty(flagActive))
                criteria.add(Restrictions.eq("Priority.flagActive", flagActive));

            LOGGER.debug(" ===- Out findAllPriorityLikeSize -=== ");
            count = Long.valueOf(criteria.list().size());
            return count;

        }catch (Exception e){
            LOGGER.error(" ===- Exception[findAllPriorityLikeSize] Msg : [{}] -=== ",e.getMessage());
            e.printStackTrace();
            return -1l;
        }

    }



}