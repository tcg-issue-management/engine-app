package com.protoss.ims.engine.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.protoss.ims.engine.model.Priority;
import com.protoss.ims.engine.model.User;

import java.util.List;
import java.util.Optional;

@Repository

public interface PriorityRepository extends JpaRepository<Priority, Long>,
                                            CrudRepository<Priority, Long>,
                                            PagingAndSortingRepository<Priority, Long> {
    List<Priority> findAll();

}
