package com.protoss.ims.engine.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.RequestParam;

import com.protoss.ims.engine.model.Project;

import java.util.List;

public interface ProjectRepository extends JpaRepository<Project, Long> {
    List<Project> findAll();

    @Query("select distinct p from Project p " +
            "where p.id = :id " +
            "")
    List<Project> findProjectById(@RequestParam("id")Long id);
}
