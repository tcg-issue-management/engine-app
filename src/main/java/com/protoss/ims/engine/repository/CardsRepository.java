package com.protoss.ims.engine.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import com.protoss.ims.engine.model.Cards;
import com.protoss.ims.engine.model.ProjectGroup;

import java.util.List;

@Repository
public interface CardsRepository extends JpaRepository<Cards, Long> {


        @Query("select distinct c from ProjectGroup  pg " +
            "left join Project p on p.id = pg.project " +
            "left join Cards c on c.groupLine = pg.displayName " +
            "where pg.project.id = :id " +
            "and c <> null " +
            "order by c.groupLine ")
    List<Cards> findCardIssueByProjectId(@RequestParam("id")Long id);

    @Query("select distinct c from Cards c " +
            "where c.projectGroup.id = :projectGroup " +
            "order by c.createdDate desc ")
    List<Cards> findCardsByProjectGroup(@RequestParam("projectGroup")Long projectGroup);

    @Query("select distinct c from Cards c " +
            "where c.projectGroup.id = :projectGroup " +
            "and c.status like CONCAT('%',lower(:status),'%') " +
            "order by c.createdDate desc ")
    List<Cards> findCardsByProjectGroupAndStatus(@RequestParam("projectGroup")Long projectGroup,@RequestParam("status")String status);


  


}
