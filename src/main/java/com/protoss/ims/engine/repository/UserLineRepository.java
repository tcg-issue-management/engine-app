package com.protoss.ims.engine.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.protoss.ims.engine.model.User;
import com.protoss.ims.engine.model.UserLine;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Optional;

/**
 * Created by ratthanan on 14/08/19.
 */
@Repository
public interface UserLineRepository extends JpaRepository<UserLine, Long> {

    UserLine findByRealIdEquals(@RequestParam("realId")String realId);
}
