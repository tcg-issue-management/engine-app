package com.protoss.ims.engine.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import com.protoss.ims.engine.model.Cards;
import com.protoss.ims.engine.model.UserGroup;
import com.protoss.ims.engine.model.UserLine;

import java.util.List;

/**
 * Created by ratthanan on 14/08/19.
 */
@Repository
public interface UserGroupRepository extends JpaRepository<UserGroup, Long> {


    @Query("select distinct ug from UserGroup ug " +
            "where ug.groupId = :groupLineId " +
            "order by ug.createdDate desc ")
    List<UserGroup> findUserGroupByGroupLineId(@RequestParam("groupLineId")String groupLineId);

}
